from time import sleep
from urllib import request

# Realizamos la conexion
resp = request.urlopen("http://mattprofe.com.ar?var=20")

# Verificamos el resultado de la conexion
if(resp.code==200):
    # Si fue 200 significa que se realizo bien
    print('OK')

    # Si emitio respuesta el sitio
    if(resp.length!='0'):
        # Leo la respuesta
        data = resp.read()
        # Decodifico la respuesta
        html = data.decode('UTF-8')
        # Muestro la respuesta
        print(html)
else:
    # El resultado de la conexion dio cualquier otro numero
    print('ERROR')
